<?php

namespace App\Services\Import;

use App\Entity\Data;
use App\Repository\DataRepository;
use Doctrine\ORM\EntityManagerInterface;

class ImportDataFromCsv implements ImportFileStrategyInterface
{
    public function importData(DataRepository $dataRepository, object $file, EntityManagerInterface $entityManagerInterface)
    {
        // Read a CSV file
        $handle = fopen($file, "r");
        $lineNumber = 0;

        // Iterate over every line of the file
        while (($raw_string = fgets($handle)) !== false) {
            if($lineNumber != 0){
                // Parse the raw csv string: "1, a, b, c"
                $row = str_getcsv($raw_string);

                $data = new Data();
                $data->setUniqueId($row[0])
                     ->setName($row[1])
                     ->setValue($row[2]);

                if(!$dataRepository->findOneByUniqueId($data->getUniqueId())){
                    $entityManagerInterface->persist($data);
                    dump("pesistence");
                    dump($lineNumber);
                    dump($row);
                }
                
            }
            // Increase the current line
            $lineNumber++;
        }

        $entityManagerInterface->flush();

        fclose($handle);
    }
}