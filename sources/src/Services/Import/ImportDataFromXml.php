<?php

namespace App\Services\Import;

use App\Entity\Data;
use App\Repository\DataRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class ImportDataFromXml implements ImportFileStrategyInterface
{
    public function importData(DataRepository $dataRepository, object $file, EntityManagerInterface $entityManagerInterface)
    {
        // Read a XML file
        $xmlFile = file_get_contents($file);
        $xmlEncoder = new XmlEncoder();
        $datas = $xmlEncoder->decode($xmlFile, 'xml');

        foreach($datas["expObj"] as $key => $value){
            $data = new Data();
            $data->setUniqueId($value["@uniqueID"])
                 ->setName($value["@name"])
                 ->setValue($value["@value"]);

            if(!$dataRepository->findOneByUniqueId($data->getUniqueId())){
                $entityManagerInterface->persist($data);
            }
        }

        $entityManagerInterface->flush();
    }
}