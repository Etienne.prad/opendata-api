<?php

namespace App\Services\Import;

use App\Repository\DataRepository;
use Doctrine\ORM\EntityManagerInterface;

interface ImportServiceInterface
{
    public function getFileExtension(Object $file) : String;
    public function setImportFileStrategy(ImportFileStrategyInterface $importFileStrategy);
    public function importData(DataRepository $dataRepository,Object $file, EntityManagerInterface $entityManagerInterface);
}