<?php

namespace App\Services\Import;

use App\Repository\DataRepository;
use Doctrine\ORM\EntityManagerInterface;

class ImportService implements ImportServiceInterface
{
    private ImportFileStrategyInterface $importFileStrategy;

    public function setImportFileStrategy(ImportFileStrategyInterface $importFileStrategy)
    {
        $this->importFileStrategy = $importFileStrategy;
    }

    public function getFileExtension(object $file) : String
    {
        return pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
    }

    public function importData(DataRepository $dataRepository, object $file, EntityManagerInterface $entityManagerInterface)
    {
        $this->importFileStrategy->importData( $dataRepository,$file, $entityManagerInterface);
    }
}