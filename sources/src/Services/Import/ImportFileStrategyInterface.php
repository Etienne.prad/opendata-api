<?php

namespace App\Services\Import;

use App\Repository\DataRepository;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Node\Expr\Cast\Object_;

interface ImportFileStrategyInterface
{
    public function importData(DataRepository $dataRepository, Object $file, EntityManagerInterface $entityManagerInterface);
}