<?php

namespace App\Controller;

use App\Entity\Upload;
use App\Form\UploadType;
use App\Repository\DataRepository;
use App\Services\Import\ImportDataFromCsv;
use App\Services\Import\ImportDataFromXml;
use App\Services\Import\ImportService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ImportController extends AbstractController
{
    /**
     * @Route("/import", name="import")
     */
    public function index( ImportService $importService, Request $request, EntityManagerInterface $entityManagerInterface, DataRepository $dataRepository): Response
    {
        $upload = new Upload();
        $form = $this->createForm(UploadType::class, $upload);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $file = $form->get('name')->getData();

            $extension = $importService->getFileExtension($file);

            if($extension === 'csv'){
                $importService->setImportFileStrategy(new ImportDataFromCsv());
            } elseif($extension === 'xml') {
                $importService->setImportFileStrategy(new ImportDataFromXml());
            }

            $importService->importData($dataRepository, $file, $entityManagerInterface);

            $this->redirectToRoute('home');
        }

        return $this->render('import/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
