<?php

namespace App\Tests\FunctionalTest\Home;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthenticationControllerTest extends WebTestCase
{
    private $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testIfLoginPageIsFunctional()
    {
        $crawler = $this->client->request('GET', '/');

        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
    }

    public function testRegistration()
    {
        $this->client->request('GET', '/register');
        $crawler = $this->client->followRedirect();

        $form = $crawler->selectButton("S'enregistrer")->form();

        // Filling form
        $form["email"] = "test2@live.fr";
        $form["password"] = "testtest";
        
        // Form submit
        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirection());
        $crawler = $this->client->followRedirect();

        $this->assertSelectorTextContains('h1', 'Se connecter');
    }

    public function testAuthentication()
    {
        $this->client->request('GET', '/');
        $crawler = $this->client->followRedirect();

        $form = $crawler->selectButton("Se connecter")->form();

        // Filling form
        $form["email"] = "test2@live.fr";
        $form["password"] = "testtest";

        // Form submit
        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirection());
        $crawler = $this->client->followRedirect();

        $this->assertSelectorTextContains('h1', 'Import de fichier');
    }
}
